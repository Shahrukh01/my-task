import React from 'react'
import "./salad.css"

function Salad() {
    return (
        <div className="col-md-7 salad">
            <div>Tossed Salad</div>
            <div>Leafy Green Salad</div>
            <div>Greek Salad</div>
        </div>
    )
}

export default Salad
