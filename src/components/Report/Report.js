import React from "react";
import { Button } from "react-bootstrap";
import "./Report.css";

function Report() {
  return (
    <div className="body offset-5 pt-2">
      <div className="row" id="row">
        <div className="col-12">
          <button className="Buttons1">
            <i class="fa fa-shopping-cart"> ORDER HISTORY</i>
          </button> &nbsp;
          <button className="Buttons2">
            <i class="fa fa-file"> X-REPORT</i>
          </button> &nbsp;
          <button className="Buttons3">
            <i class="fa fa-th"> OPEN ORDERS </i>
          </button>&nbsp;
        </div>
      </div>
      <div className="row pt-2">
        <div className="col-12">
          <button className="Buttons4">
            <i class="fa fa-group"> NEW MEMBERS</i>
          </button> &nbsp; 
          <button className="Buttons5">
            <i class="fa fa-calendar"> MY SCHEDULE</i>
          </button> &nbsp; 
        </div>
      </div>
    </div>
  );
}

export default Report;
