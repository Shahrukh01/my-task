import React,{useState} from 'react'
import Sidebar from '../sidebar/Sidebar'
import Tables from '../tables/Tables'
import Menu from '../MenuItems/Menu'
import Burger from '../MenuItems/burger/Burger'
import Chicken from '../MenuItems/chicken/Chicken'
import Salad from '../MenuItems/salad/Salad'
import "./dashboard.css"
function Dashboard() {
const [state, setState] = useState("home")
const [counter, setCounter] =useState(0);

const counterHandler = ()=>{
  setCounter(counter+1)
}
const burgerHandler = ()=>{
        setState("burger")
}
const chickenHandler = ()=>{
  setState("chicken")
}
const saladHandler = ()=>{
  setState("salad")
}
  const menuHandler = ()=>{
  setState("menu")
}
 const homeHandler = ()=>{
  setState("home")
}
  return (
    <div className="container-fluid">
      <div className= "row ">
      <Sidebar counter={counter}/>
      {state === "burger" ? <Burger counterHandler={counterHandler}/>: 
      state === "chicken" ? <Chicken/>: state ==="salad" ?<Salad />:
       state ==="home" ?<Tables menuHandler={menuHandler} />: state ==="menu" && <Menu burgerHandler={burgerHandler}  chickenHandler={chickenHandler}  saladHandler={saladHandler} />
      
      
      }
      
         
        

      </div>
      {/* <button className="btn btn-dark mb-10  " onClick={menuHandler}>menu</button>
      <button className="fa fa-table btn btn-info mb-10 ml-2" onClick={homeHandler}></button> */}
    </div>
  )
}

export default Dashboard;
