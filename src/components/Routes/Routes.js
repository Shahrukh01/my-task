import React from "react";
import { Route, Switch } from "react-router-dom";
import Report from "../Report/Report";
function Routes(props) {
  let {history} = props;
  return (
    <div>
    <Switch>
    
     <Route path="/report" component={Report}   /> 
     
    </Switch>
    </div>
  );
}

export default Routes;
