import React from 'react'
import "./sidebar.css"

function Sidebar() {
    return (

         <div className="col-lg-4 col-md-6 sidebar">
            <h4 className="mt-5">Operator: Admin</h4>
            <div className="table-responsive table-bordered table-custom ">
            <table class="table table-sm ">
  <thead>
    <tr>
      <th scope="col" className='text-left'>RECIPIE</th>
      <th scope="col" className='text-left'>USER</th>
      <th scope="col" className='text-left'>AMT</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th className='text-left'>Zinger</th>
      <td className='text-left'>4.00</td>
      <td className='text-left'>16.00</td>
   
    </tr>
    <tr>
      <th className='text-left'>Chicken</th>
      <td className='text-left'>26.00</td>
      <td className='text-left'>182.00</td>
    
    </tr>
    <tr>
      <th className='text-left'>Chicken</th>
      <td className='text-left'>26.00</td>
      <td className='text-left'>182.00</td>
    
    </tr>
    <tr>
      <th className='text-left'>Chicken</th>
      <td className='text-left'>26.00</td>
      <td className='text-left'>182.00</td>
    
    </tr>

    <tr>
      <th className='text-left'>Salad</th>
      <td className='text-left'>2.00</td>
      <td className='text-left'>8.00</td>
    
    </tr>
    <tr>
      <th className='text-left'>Salad</th>
      <td className='text-left'>2.00</td>
      <td className='text-left'>8.00</td>
    
    </tr>
  </tbody>
</table>
                    </div>
                    
                    <hr/>
                    <div className="calculation-buttons">
                    <button><i className="fa fa-close "></i ></button>
                    <button><i className="fa fa-plus"></i></button>
                    <button><i className="fa fa-minus"></i></button>
                    <button className="qty">Qty</button>
                   
                    </div>
                    <div className="functional-buttons ">
                    <div>
                    <button> clear receipt</button>
                    <button> Modefier</button>
                    <button> Send</button>
                    </div>
                    <div className="mt-2">
                        <label className="order-left-items">item total</label>
                        <label className="order-right-items">0</label>
                        <label className="order-left-items">Discount</label>
                        <label className="order-right-items">0</label>
                        <label className="order-left-items">sub total</label>
                        <label className="order-right-items">0</label>
                        <label className="order-left-items">tax</label>
                        <label className="order-right-items">0</label>
                        <label className="order-left-items">points</label>
                        <label className="order-right-items">0</label>
                        <label className="order-left-items">total</label>
                        <label className="order-right-items">0</label>
                         
                    </div>



            </div>
        </div>
        
    )
}

export default Sidebar
