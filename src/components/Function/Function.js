import React from "react";
import "./Function.css";
function Function() {
  return (
    <div className="body offset-5 pt-2">
      <div className="row" id="row2">
        <div className="col-12">
          <button className="buttons1">
            <i class="fa fa-tasks"> RESERVATIONS</i>
          </button>&nbsp;
          <button className="buttons2">
            <i class="fa fa-calendar"> TIME CLOCK</i>
          </button>&nbsp;
          <button className="buttons3">
            <i class="fa fa-sign-out"> CLOSE REGISTERS</i>
          </button>&nbsp;
        </div>
        <div className="row pt-2">
          <div className="col-12">
            <button className="buttons4">
              <i class="fa fa-money"> TIPS</i>
            </button>&nbsp;
            <button className="buttons5">
              <i _ class="fa fa-sticky-note">
                LOAD RECEIPT
              </i>
            </button>&nbsp;
          </div>
        </div>
      </div>
    </div>
  );
}

export default Function;
