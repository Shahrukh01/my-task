import React from "react";
import {  FiPercent, FaSitemap, BsFillPrinterFill, BiMoney, AiOutlineTable, MdMenuBook } from "react-icons/all";

import "./Footer.css";
function Footer() {
  return (
    <div>
      <footer>
      
        <div className="footer">
          <div  className=" container-fluid">
          <div  id="btn">
            <div className="row">
              <div className="col-lg-8 col-md-12 ">
                <div className="btn1 p-2">
            <button  className="bottom-button ">
            <FiPercent/>
              <br />  
              Discount
            </button>&nbsp;
            <button  className="bottom-button">
<FaSitemap/>
              <br />
              Split
            </button>&nbsp;
            <button  className="bottom-button ">
    <BsFillPrinterFill/>
              <br  />
              Print
            </button>&nbsp;
            <button  className="bottom-button ">
            <BiMoney/>
              <br  />
              Split
            </button>&nbsp;
            <button  className="bottom-button ">
            <AiOutlineTable/>
              <br  />
              Table
            </button>&nbsp;
            <button  className="bottom-button  ">
            <MdMenuBook/>
            <br />
                Menu
                
                </button>&nbsp;
            </div>
          </div>
          <div className="col-md-4 pr-5">
          <div  className="abc">
            <p  className="p">
            <b> Patron </b> 
            </p>
            <p  className="works">
             <b> works </b>
            </p>
          </div>
        </div>
        </div>
      
        </div>
        </div>
        </div>
      </footer>
      
    </div>
  );
}

export default Footer;
