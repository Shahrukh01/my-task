import React, { useState } from "react";
import { Button, Navbar, Container, Nav } from "react-bootstrap";
import { useHistory } from "react-router-dom";

import "./Header.css";
import { Modal } from "react-bootstrap";
import {
  FaSearchPlus,
  FaGlobeAsia,
  FaUserPlus,
  FaUserAlt,
  FaCogs,
} from "react-icons/fa";
import { HiDocumentReport } from "react-icons/all";

function Header() {
  let history = useHistory();
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <div className="main">
      <Modal id="modal" show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>
            <h5>Login</h5>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {" "}
          <form action="" method="post">
            <div className="modal-body">
              <div className="form-group">
                <label>
                  <h6>Email address</h6>
                </label>
                <input
                  type="text"
                  className="form-control"
                  required="required"
                />
              </div>
              <div className="form-group">
                <div className="clearfix">
                  <label>
                    <h6>Password</h6>
                  </label>
                  <a href="#" className="float-right text-muted " id="forget">
                    <h6> Forgot Password </h6>
                  </a>
                </div>

                <input
                  type="password"
                  class="form-control"
                  required="required"
                />
              </div>
            </div>
            <div className="modal-footer">
              <label className="form-check-label">
                <input type="checkbox" /> Remember me
              </label>
              <input type="submit" className="btn btn-info " value="Login" />
            </div>
          </form>
        </Modal.Body>
        
      </Modal>
        
      <Navbar collapseOnSelect expand="md" variant="dark">
        <Container fluid>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="me-auto ">
              
              <Button
                variant=""
                className="top-button login_btn  custom_btn"
                onClick={handleShow}
              >
                <FaGlobeAsia />Login
              </Button>&nbsp;
              <Button variant="light" className="top-button custom_btn">
                <FaSearchPlus />Lookup
              </Button>&nbsp;
              <Button variant="light" className="top-button custom_btn" id="New">
                <FaUserPlus />New
              </Button>&nbsp;
              <Button variant="light" className="top-button custom_btn">
                <FaUserAlt />Guest
              </Button>&nbsp;
              
            </Nav>
            <Nav className="rightSide ">
              <Button onClick={() => { history.push("/Report")}} variant="light" className="top-button custom_btn">
                <HiDocumentReport />Report
              </Button>&nbsp;
              <Button variant="light" className="top-button custom_btn">
                <FaCogs />Function
              </Button>&nbsp;
              
            </Nav>
            
          </Navbar.Collapse>
        </Container>
      </Navbar>
      </div>
      
  );
}

export default Header;
