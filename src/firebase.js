import firebase from 'firebase'


const firebaseConfig = {
    apiKey: "AIzaSyB77MDI_M5cm_i2QCnNi0N8-tK_-aA6xd0",
    authDomain: "patro-cab6c.firebaseapp.com",
    projectId: "patro-cab6c",
    storageBucket: "patro-cab6c.appspot.com",
    messagingSenderId: "387674846455",
    appId: "1:387674846455:web:743a80d73f6e4d166f8ff2"
  };

  const firebaseApp = firebase.intializeApp
  (firebaseConfig);
  const db = firebase.firestore();
  const auth = firebase.auth();
  export {auth};
  export default db;
